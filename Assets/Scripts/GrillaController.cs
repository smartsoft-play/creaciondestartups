﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GrillaController : MonoBehaviour {//GrillaControllerPlayer(cafesito)
	public GameObject gridObject;
	public UIScrollView scrollView;
	public GameObject objCentered;
	public UIButton leftButton;
	public UIButton rightButton;

	[SerializeField]
	private List <GameObject> objsInfo;

	// Use this for initialization
	void Start () {
		foreach (Transform Actualitem in gridObject.transform)
		{
				objsInfo.Add(Actualitem.gameObject);
		}
		objCentered = objsInfo[0];
	
		//leftButton.gameObject.SetActive(false);
		IComparer c = new CaseInsensitiveComparer();
		
		for(int i = 0 ; i < objsInfo.Count; i ++){
			for(int j = i+1 ; j < objsInfo.Count; j++){
				if(c.Compare(objsInfo[j].name , objsInfo[i].name) == -1){
					GameObject temp = objsInfo[i];
					objsInfo[i] = objsInfo[j];
					objsInfo[j] = temp;
				}
			}
		}


		EventDelegate.Add(rightButton.onClick, delegate(){		
			RightButton(); });
		
		EventDelegate.Add(leftButton.onClick, delegate(){
			LeftButton();});

		gridObject.GetComponent<UICenterOnChild>().onFinished += OnVisibleDirectionButtons;
	}
	
	// Update is called once per frame
	void Update ()
	{
		objCentered = gridObject.GetComponent<UICenterOnChild>().centeredObject;

	}

	public void LeftButton()
	{
		for (int i = 0; i < objsInfo.Count; i++) //recorrer lista objsinfo
		{
			//comparar si el objecto centrado es igual al elemento que se esta recoriendo en ese momento, condicion, si el objecto centrado es diferente del primer
			//elemento de la lista. Si se cumple la condicion se activa el evento Onclick del elemente que este a la izquierda porque estamos en el metodo izq
			if(objCentered.Equals (objsInfo[i]) && !objCentered.Equals(objsInfo[0]))
			{
				objsInfo[i-1].SendMessage("OnClick");
			}
		}
	}
	public void RightButton()
	{
		for (int i = 0; i < objsInfo.Count; i++) //recorrer lista objsinfo
		{
			//comparar si el objecto centrado es igual al elemento que se esta recoriendo en ese momento, condicion, si el objecto centrado es diferente del primer
			//elemento de la lista. Si se cumple la condicion se activa el evento Onclick del elemente que este a la derecha porque estamos en el metodo derec
			if(objCentered.Equals (objsInfo[i]) && !objCentered.Equals(objsInfo[objsInfo.Count-1]))
			{
				objsInfo[i+1].SendMessage("OnClick");
			}
		}
	}
	public void OnVisibleDirectionButtons ()
	{
		if(objCentered.Equals(objsInfo[0]))
		{
			leftButton.gameObject.SetActive(false);
			rightButton.gameObject.SetActive(true);
		}
		else if(objCentered.Equals(objsInfo[objsInfo.Count -1]))
		{
			leftButton.gameObject.SetActive(true);
			rightButton.gameObject.SetActive(false);
		}
		else
		{
			leftButton.gameObject.SetActive(true);
			rightButton.gameObject.SetActive(true);
		}
	}



}
