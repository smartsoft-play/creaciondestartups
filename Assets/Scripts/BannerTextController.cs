﻿using UnityEngine;
using System.Collections;

public class BannerTextController : MonoBehaviour {
	public UILabel lblBanner;
	// Use this for initialization
	void Start () {
		lblBanner = gameObject.GetComponent<UILabel> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void LoadBannerInfo(string info)
	{
		lblBanner.text = info;
	}
}
