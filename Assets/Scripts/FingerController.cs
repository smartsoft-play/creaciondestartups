﻿using UnityEngine;
using System.Collections;

public class FingerController : MonoBehaviour {
	public TweenPosition twCoverPage;
	public TweenPosition twGuia;
	public TweenPosition twBackCover;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.D)) {

			PlayerPrefs.DeleteAll();
		}
		if (Input.GetKeyDown (KeyCode.Escape)) {
			
			Application.Quit();
		}
	}

	void OnSwipe( SwipeGesture gesture ) 
	{
		Vector2 move = gesture.Move;
		
		float velocity = gesture.Velocity;
		
		FingerGestures.SwipeDirection direction = gesture.Direction;

		//if (grounded == false) {
		if (direction == FingerGestures.SwipeDirection.Right)
		{
				
				

			if(twGuia.gameObject.transform.localPosition.x == 750)//mover cover
			{
				twCoverPage.PlayReverse();
			}

			if(twGuia.gameObject.transform.localPosition.x == 0 && twBackCover.gameObject.transform.localPosition.x == 846)//mover guia desde app screen
			{
				twGuia.PlayReverse();
			}
			if(twBackCover.gameObject.transform.localPosition.x == 0 && twGuia.gameObject.transform.localPosition.x == 0)
			{
				twBackCover.PlayReverse();
			}

		}

		if (direction == FingerGestures.SwipeDirection.Left)
		{
				
			twCoverPage.PlayForward();


			if(twCoverPage.gameObject.transform.localPosition.x == -750f)
			{
				twGuia.PlayForward();
			}
			if(twBackCover.gameObject.transform.localPosition.x == 846 && twGuia.gameObject.transform.localPosition.x == 0)
			{
				twBackCover.PlayForward();
			}

				
		}



	}
	public void ContForeSprite(int position)//para detectar en que momento se encuentra la camara, o qu eesta enfocando
	{//4 pantallas: portada, guia, app, contraportada
		Debug.Log ("mmi position" +position );
		if (position == 3 && twGuia.gameObject.transform.position.x == 0)
		{
			//twGuia.from.x = 0f;
			//twGuia.to.x = -782f;
		}
	}
}
