﻿using UnityEngine;
using System.Collections;

public class GesturesController : MonoBehaviour {
	public float zoomSpeed = 5.0f;
	public float minZoomAmount = 0;
	public float maxZoomAmount = 50;
	public float SmoothSpeed = 4.0f;
	
	Vector3 defaultPos = Vector3.zero;
	float defaultFov = 0;
	float defaultOrthoSize = 0;
	float idealZoomAmount = 0;
	float zoomAmount = 0;

	public enum ZoomMethod
	{
		// move the camera position forward/backward
		Position,
		
		// change the field of view of the camera, or projection size for orthographic cameras
		FOV,
	}
	
	public ZoomMethod zoomMethod = ZoomMethod.Position;

	public float IdealZoomAmount
	{
		get { return idealZoomAmount; }
		set { idealZoomAmount = Mathf.Clamp( value, minZoomAmount, maxZoomAmount ); }
	}

	public float ZoomAmount
	{
		get { return zoomAmount; }
		set
		{
			zoomAmount = Mathf.Clamp( value, minZoomAmount, maxZoomAmount );
			
			switch( zoomMethod )
			{
			case ZoomMethod.Position:
				transform.position = defaultPos + zoomAmount * transform.forward;
				break;
				
			case ZoomMethod.FOV:
				if( GetComponent<Camera>().orthographic )
				{
					GetComponent<Camera>().orthographicSize = Mathf.Max( defaultOrthoSize - zoomAmount, 0.1f );
				}
				else
				{
					//CameraFov = Mathf.Max( defaultFov - zoomAmount, 0.1f );
				}
				break;
			}
		}
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		ZoomAmount = Mathf.Lerp( ZoomAmount, IdealZoomAmount, Time.deltaTime * SmoothSpeed );

	}
	void OnPinch( PinchGesture gesture ) 
	{
		Debug.Log ("Pinch");
		//IdealZoomAmount += zoomSpeed * gesture.Delta.Centimeters();
		GetComponent<Camera>().orthographicSize = Mathf.Max( defaultOrthoSize - zoomAmount, 0.1f );
	}
	void OnTap(TapGesture gesture)
	{
		Debug.Log ("Tapping");

	}
}
