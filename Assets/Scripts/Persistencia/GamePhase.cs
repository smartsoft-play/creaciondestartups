﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class GamePhase
{
	public int [] phases;
	//public GameObject [] objPhases  = new GameObject[5];
	//public UIButton[] tempCheckList;
	public int contNotification;//to show in numberNotification...300, 200,100,200s.s,200mine

	
	public string NameOfPhase;
	//public bool stateBell = false;
	public int clickedPhase;
	public int lastPhase;//mismo id button

	public GamePhase(int [] phases_,int contNotification_,string namePhase_,int clickedPhase_,int lastPhase_)
	{
		this.phases = phases_;
		this.contNotification = contNotification_;
		this.NameOfPhase = namePhase_;
		this.clickedPhase = clickedPhase_;
		this.lastPhase = lastPhase_;
	}

}
