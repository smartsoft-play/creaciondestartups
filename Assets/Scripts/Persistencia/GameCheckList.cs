﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class GameCheckList
{
	public int [] checklistButtons;
	public int allChecklist;
	public int temp;
	public int myId;
	public int contCheck;
	public GameCheckList(int []checklistButtons_, int allChecklist_, int temp_,int myId_ , int contCheck_)
	{
		this.checklistButtons = checklistButtons_;
		this.allChecklist = allChecklist_;
		this.temp = temp_;
		this.myId = myId_;
		this.contCheck = contCheck_;
	}
}
