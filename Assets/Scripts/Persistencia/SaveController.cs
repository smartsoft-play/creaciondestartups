﻿using UnityEngine;
using System.Collections;

public class SaveController : MonoBehaviour {//agregar en cada scroll 
	public PhasesController compPhasesController;
	// Use this for initialization
	void Start () {
		compPhasesController = GetComponent<PhasesController> ();
	}
	
	public void SaveData()
	{
		//Guardar datos
		//guarda variables de este scritp
		SaveLoadGame.phaseParameters.Clear ();
		SaveLoadGame.phaseParameters = new System.Collections.Generic.List<GamePhase> ();
		//GamePhase objPhase = new GamePhase (compPhasesController.contNotification,compPhasesController.NameOfPhase,compPhasesController.clickedPhase, compPhasesController.lastPhase);
		//SaveLoadGame.phaseParameters.Add (objPhase);
		//guardar los datos de de cada scrollview, del scripts checklistController
		
		SaveLoadGame.phaseParameters.Clear ();
		SaveLoadGame.checkListParameters = new System.Collections.Generic.List<GameCheckList> ();
		if (compPhasesController.objPhases.Length > 0) {
			for (int i = 0; i < compPhasesController.objPhases.Length; i++) {
				GameCheckList objChecklist = new GameCheckList(compPhasesController.objPhases[i].GetComponent<CheckListController>().checklistButtons,compPhasesController.objPhases[i].GetComponent<CheckListController>().allChecklist,compPhasesController.objPhases[i].GetComponent<CheckListController>().temp,compPhasesController.objPhases[i].GetComponent<CheckListController>().myId,compPhasesController.objPhases[i].GetComponent<CheckListController>().contCheck);
				SaveLoadGame.checkListParameters.Add(objChecklist);
			}
		}
		SaveLoadGame.Save ();
	}
}
