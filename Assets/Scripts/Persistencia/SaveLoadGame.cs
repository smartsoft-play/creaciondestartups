﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public static class SaveLoadGame 
{
	public static List<GamePhase> phaseParameters = new List<GamePhase> ();
	public static List<GameCheckList>checkListParameters = new List<GameCheckList>();

	public static void Save()
	{
		var b = new BinaryFormatter ();
		var f = File.Create (Application.persistentDataPath + "highscore.dat");
		b.Serialize (f,phaseParameters);
		b.Serialize (f,checkListParameters);
		//b.Serialize (f,phaseParameters);
		Debug.LogWarning("entranso a guardar en save load");//Actual muestra debug para indicar si no entra a la condicion
		f.Close ();
	}
	public static void Load()
	{
		if(File.Exists(Application.persistentDataPath + "highscore.dat"))
		{
			var b = new BinaryFormatter();
			var f = File.Open(Application.persistentDataPath + "highscore.dat", FileMode.Open);
			phaseParameters = (List<GamePhase>)b.Deserialize(f);
			checkListParameters = (List<GameCheckList>)b.Deserialize(f);
			PhasesController.loadObjects = true;
			Debug.LogWarning("entrandoooooooo");//Actual muestra debug para indicar si no entra a la condicion
			f.Close();
		}
		else
		{
			Debug.LogWarning("vvvvvvvvvvvvvvvvvv");//Actual muestra debug para indicar si no entra a la condicion
		}
	}
}
