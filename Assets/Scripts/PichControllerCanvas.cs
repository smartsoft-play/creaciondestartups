﻿using UnityEngine;
using System.Collections;

public class PichControllerCanvas : MonoBehaviour {
	public Canvas MainCamera;
	bool pinching = false;
	public float pinchScaleFactor = 0.02f;
	// Use this for initialization
	void Start () {
	
	}
	bool Pinching
	{
		get { return pinching; }
		set
		{
			if( pinching != value )
			{
				pinching = value;
				//UpdateTargetMaterial();
			}
		}
	}
	// Update is called once per frame
	void Update () {
		Debug.Log("ScaleFactor" + MainCamera.scaleFactor);
	}
	void OnPinch( PinchGesture gesture )
	{
		if( gesture.Phase == ContinuousGesturePhase.Started )
		{
			Pinching = true;
		}
		else if( gesture.Phase == ContinuousGesturePhase.Updated )
		{
			if( Pinching && MainCamera.scaleFactor >= 1f && MainCamera.scaleFactor <= 3.2f)
			{
				// change the scale of the target based on the pinch delta value
				//target.transform.localScale += gesture.Delta * pinchScaleFactor * Vector3.one;
				MainCamera.scaleFactor += gesture.Delta * pinchScaleFactor;
			}
			if(MainCamera.scaleFactor >= 3)
				MainCamera.scaleFactor = 3;
			if(MainCamera.scaleFactor <= 1)
				MainCamera.scaleFactor = 1;
		}
		else
		{
			if( Pinching )
			{
				Pinching = false;
			}
		}
	}
}
