﻿using UnityEngine;
using System.Collections;

public class PhasesController : MonoBehaviour {//
	public int [] phases = new int[5];
	public GameObject [] objPhases  = new GameObject[5];//
	public UIButton[] tempCheckList;//

	public TweenRotation twBell;
	public UILabel lblnumberNotification;//show number of notification, max 3 min0, show contNotification

	public int contNotification;//to show in numberNotification

	public string first;
	public string second;
	public string third;

	public string NameOfPhase;

	public UILabel lblUp;
	public UILabel lblCenter;
	public UILabel lblBotoon;

	public UIPanel panelControll;

	public bool stateBell = false;
	public TweenAlpha twOpacity;

	public int clickedPhase;
	public int lastPhase;//mismo id button
	public GameObject arrow;

	[SerializeField]
	public static bool loadObjects = false;

	void Awake () {
		SaveLoadGame.Load ();


		Debug.Log ("Estado de bton" +  PlayerPrefs.GetInt("ActualStateButton"));
		//cargar datos
	/*	if(loadObjects)
		{
			Debug.Log("Mensaje recibido para cargar******");
			Debug.Log("phases prarameteres******" + SaveLoadGame.phaseParameters);
			loadObjects = false;
		if(PlayerPrefs.GetInt("AllChecklist") == 1)
			{
				/*phases = SaveLoadGame.phaseParameters[0].phases;
				contNotification = SaveLoadGame.phaseParameters[0].contNotification;
				NameOfPhase = SaveLoadGame.phaseParameters[0].NameOfPhase;
				clickedPhase = SaveLoadGame.phaseParameters[0].clickedPhase;
				lastPhase = SaveLoadGame.phaseParameters[0].lastPhase;

				//cargar datos de checklist en cada uno de los scrollview
				for (int i = 0; i < SaveLoadGame.checkListParameters.Count; i++) {//
					

					objPhases[i].GetComponent<CheckListController>().checklistButtons = SaveLoadGame.checkListParameters[i].checklistButtons;
					objPhases[i].GetComponent<CheckListController>().allChecklist = SaveLoadGame.checkListParameters[i].allChecklist;
					objPhases[i].GetComponent<CheckListController>().temp = SaveLoadGame.checkListParameters[i].temp;
					objPhases[i].GetComponent<CheckListController>().myId = SaveLoadGame.checkListParameters[i].myId;
					objPhases[i].GetComponent<CheckListController>().contCheck = SaveLoadGame.checkListParameters[i].contCheck;
				}
			}
		}*/
	}
	void Start()
	{
		Debug.Log ("temp de phases 0_" + PlayerPrefs.GetInt ("EndCheck0"));
		if (PlayerPrefs.GetInt ("EndCheck0") == 2) {
			phases[0] = 1;
		}
		if (PlayerPrefs.GetInt ("EndCheck1") == 7) {
			phases[1] = 1;
		}
		if (PlayerPrefs.GetInt ("EndCheck2") == 3) {
			phases[2] = 1;
		}
		if (PlayerPrefs.GetInt ("EndCheck3") == 6) {
			phases[3] = 1;
		}

	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.D)) {


			PlayerPrefs.DeleteKey("ActualStateButton");
			PlayerPrefs.DeleteAll();
		}
	
	}
	public void VerifyPhases(int idButton)// verifica en que estado las fases, entra desde cada boton que activa o entra a las fases,,, idButton igual a la posicion anterior del array----> desde cada boton 
	{//idbutton recibe de parametro el id de la fase anterior, botones de 0 a 5, ejemplo si soy boton 2 recibe 1

		//Guardar datos
		//guarda variables de este scritp
		/*
		SaveLoadGame.phaseParameters.Clear ();
		SaveLoadGame.phaseParameters = new System.Collections.Generic.List<GamePhase> ();
		GamePhase objPhase = new GamePhase (phases,contNotification,NameOfPhase,clickedPhase, lastPhase);
		SaveLoadGame.phaseParameters.Add (objPhase);
		//guardar los datos de de cada scrollview, del scripts checklistController

		SaveLoadGame.phaseParameters.Clear ();
		SaveLoadGame.checkListParameters = new System.Collections.Generic.List<GameCheckList> ();
		if (objPhases.Length > 0) {
			for (int i = 0; i < objPhases.Length; i++) {
				GameCheckList objChecklist = new GameCheckList(objPhases[i].GetComponent<CheckListController>().checklistButtons,objPhases[i].GetComponent<CheckListController>().allChecklist,objPhases[i].GetComponent<CheckListController>().temp,objPhases[i].GetComponent<CheckListController>().myId,objPhases[i].GetComponent<CheckListController>().contCheck);
				SaveLoadGame.checkListParameters.Add(objChecklist);
			}
		}
		SaveLoadGame.Save ();
		*/
		// fin de guardar

		if (idButton != 10) {//222
			clickedPhase = idButton + 1;//2222

		}
		if (idButton == 10) {
			arrow.SetActive(false);		
		}
		lastPhase = idButton;
		switch (idButton) // nombre de la fase que va a recomendar terminar 
		{
			case 0:
				NameOfPhase = "Ideación";
				break;
			case 1:
				NameOfPhase = "Customer discover";
				break;
			case 2:
				NameOfPhase = "Customer validation";
				break;
			case 3:
				NameOfPhase = "Customer creation";
				break;
				
		}


		if(idButton < 10)//diferente al boton 1
		{
			if (phases[idButton] != 1) //si la fase anterior es diferente a uno notifique que la termine
			{
				Debug.Log("Notificacion**** termina la fase anterior");	
				twBell.PlayForward();


				if(contNotification <3){
					contNotification += 1;}

				lblnumberNotification.text = contNotification.ToString();
				third = second;
				second = first;
				first = "Antes de continuar completar la fase de " + NameOfPhase;
			}
			else
			{
				Debug.Log("Notificacion**** terminaste la fase anterior");			
			}

		}
		NotificationBell ();
		Invoke ("FindChecklist",0.2f);
	}
	public void NotificationBell()
	{

		switch (contNotification)
		{
		case 1://configurar scrollview para que solo se vea el primer cuadro de texto de notificacion, los otros estan vacio
			panelControll.baseClipRegion  = new Vector4(21,27,374,162);
			lblnumberNotification.gameObject.transform.parent.gameObject.SetActive(true);//activar el padre
			twBell.gameObject.GetComponent<UIButton>().isEnabled = true;
			break;
		case 2://configurar scrollview para que solo se vea el primer cuadro de texto de notificacion, los otros estan vacio
			panelControll.baseClipRegion  = new Vector4(21,-10,374,236);
			break;
		case 3://configurar scrollview para que solo se vea el primer cuadro de texto de notificacion, los otros estan vacio 
			panelControll.baseClipRegion  = new Vector4(21,-49,374,314);
			break;

		}
	

		lblUp.text = first;
		lblCenter.text = second;
		lblBotoon.text = third;
	}
	public void FindChecklist()// Habilitar o desabilitar los checklist
	{
		tempCheckList = objPhases [clickedPhase].transform.GetComponentsInChildren<UIButton> ();
		if(lastPhase != 10){
			if (phases[lastPhase] != 1) //si la fase anterior es diferente a uno 
			{
				arrow.SetActive(true);
				foreach (UIButton item in tempCheckList)
				{
					item.isEnabled = false;
				}
			}
			else
			{
				arrow.SetActive(true);
				foreach (UIButton item in tempCheckList)
				{
					item.isEnabled = true;
				}
			}
		}
	}
	public void ShowNotification()//cuando se clickea en la campana
	{
		stateBell = !stateBell;
		twOpacity.PlayForward();
		panelControll.gameObject.SetActive (stateBell);
		contNotification = 0;
		lblnumberNotification.gameObject.transform.parent.gameObject.SetActive(false);//desactivar el padre
		if (panelControll.gameObject.activeInHierarchy == false)
		{
				twBell.gameObject.GetComponent<UIButton> ().isEnabled = false;
			twOpacity.PlayReverse();
		}
	}
}
