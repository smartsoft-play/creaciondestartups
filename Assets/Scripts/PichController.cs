﻿using UnityEngine;
using System.Collections;

public class PichController : MonoBehaviour {
	public Camera MainCamera;
	bool pinching = false;
	public float pinchScaleFactor = 0.02f;
	// Use this for initialization
	void Start () {
	
	}
	bool Pinching
	{
		get { return pinching; }
		set
		{
			if( pinching != value )
			{
				pinching = value;
				//UpdateTargetMaterial();
			}
		}
	}
	// Update is called once per frame
	void Update () {
	
	}
	void OnPinch( PinchGesture gesture )
	{
		if( gesture.Phase == ContinuousGesturePhase.Started )
		{
			Pinching = true;
		}
		else if( gesture.Phase == ContinuousGesturePhase.Updated )
		{
			if( Pinching )
			{
				// change the scale of the target based on the pinch delta value
				//target.transform.localScale += gesture.Delta * pinchScaleFactor * Vector3.one;
				MainCamera.orthographicSize += gesture.Delta * pinchScaleFactor;
			}
		}
		else
		{
			if( Pinching )
			{
				Pinching = false;
			}
		}
	}
}
